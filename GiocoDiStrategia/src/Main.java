import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Unit gattoSoldato1 = new Unit("Gatto Soldato",5,1,false);
        Unit gattoSoldato2 = new Unit("Gatto Soldato",5,1,false);
        Unit gattoMago1 = new Unit("Gatto Mago",1,5,false);
        Unit gattoMago2 = new Unit("Gatto Mago",1,5,false);
        Unit superGatto1 = new Unit("SUPER Gatto",10,2,false);

        Unit gattoSoldato3 = new Unit("Gatto Soldato",5,1,false);
        Unit gattoSoldato4 = new Unit("Gatto Soldato",5,1,false);
        Unit gattoMago3 = new Unit("Gatto Mago",1,5,false);
        Unit gattoMago4 = new Unit("Gatto Mago",1,5,false);
        Unit superGatto2 = new Unit("SUPER Gatto",10,2,false);

        Army armata1 = new Army(List.of(gattoSoldato1,gattoSoldato2,gattoMago1,gattoMago2,superGatto1));
        Army armata2 = new Army(List.of(gattoSoldato3,gattoSoldato4,gattoMago3,gattoMago4,superGatto2));

        boolean partitaInCorso = true;

        Scanner scanner = new Scanner(System.in);

        while (partitaInCorso){
            System.out.println("Giocatore 1, decidi cosa vuoi fare? 1 Posiziona nuova pedina. 2 Muovi pedina. 3 Attacca");
            int sceltaGiocatore = scanner.nextInt();
            switch (sceltaGiocatore){
                case 1 :
                    System.out.println("Ok, questa è la lista di pedine che puoi posizionare: ");
                    for (int i = 0; i < armata1.unitaPosizionabili().size(); i++) {
                        Unit unita = armata1.unitaPosizionabili().get(i);
                        System.out.println((i + 1) + " - " + unita.getNome() + " " + unita.getForza() + " " + unita.getSalute());
                    }
                    break;
                case 2 :
                    break;
                case 3 :
                    break;
                default:
                    System.out.println("Scelta non consentita, hai perso il turno. =(");
                    break;
            }
        }

    }
}
