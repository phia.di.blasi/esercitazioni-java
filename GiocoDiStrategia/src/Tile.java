public class Tile {
    private boolean casellaOccupata;

    public Tile(boolean casellaOccupata) {
        this.casellaOccupata = casellaOccupata;
    }

    public boolean isCasellaOccupata() {
        return casellaOccupata;
    }

    public void setCasellaOccupata(boolean casellaOccupata) {
        this.casellaOccupata = casellaOccupata;
    }
}
