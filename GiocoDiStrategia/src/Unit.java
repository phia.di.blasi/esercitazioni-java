public class Unit {
    private String nome;
    private int salute;
    private int forza;
    private boolean posizionata;

    public Unit(String nome, int salute, int forza, boolean posizionata) {
        this.nome = nome;
        this.salute = salute;
        this.forza = forza;
        this.posizionata = posizionata;
    }

    public String getNome() {
        return nome;
    }

    public int getSalute() {
        return salute;
    }

    public int getForza() {
        return forza;
    }

    public boolean isPosizionata() {
        return posizionata;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSalute(int salute) {
        this.salute = salute;
    }

    public void setForza(int forza) {
        this.forza = forza;
    }

    public void setPosizionata(boolean posizionata) {
        this.posizionata = posizionata;
    }
    //Metodo per capire se una unità è viva
    public boolean unitaViva(){
        return this.salute > 0;
    }
    public void danniSubiti(int danni){
        this.salute = this.salute - danni;
    }
}
