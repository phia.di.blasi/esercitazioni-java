import java.util.ArrayList;
import java.util.List;

public class Army {
    private List<Unit> listaDiUnita;

    public Army(List<Unit> listaDiUnita) {
        this.listaDiUnita = listaDiUnita;
    }

    public List<Unit> getListaDiUnita() {
        return listaDiUnita;
    }

    public void setListaDiUnita(List<Unit> listaDiUnita) {
        this.listaDiUnita = listaDiUnita;
    }
    //Metodo per avere la lista delle unità posizionabili
    public List<Unit> unitaPosizionabili(){
        List<Unit> unitaPosizionabili = new ArrayList<>();
        for (Unit unita : this.listaDiUnita) {
            if (!unita.isPosizionata()){
                unitaPosizionabili.add(unita);
            }
        }
        return unitaPosizionabili;
    }
    //Metodo che ci restituisce le unità movibili perchè già posizionate sulla Mappa
    public List<Unit> unitaMovibili(){
        List<Unit> unitaMovibili= new ArrayList<>();
        for (Unit unita : this.listaDiUnita) {
            if (unita.isPosizionata()){
                unitaMovibili.add(unita);
            }
        }
        return unitaMovibili;
    }
    public void rimuoviUnita(Unit unitaDaRimuovere){
        this.listaDiUnita.remove(unitaDaRimuovere);
    }
}

