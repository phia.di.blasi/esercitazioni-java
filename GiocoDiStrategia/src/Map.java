public class Map {
    private Tile[][] mappaDiGioco;

    //inizializzo una matrice dove definisco altezza e larghezza di essa.
    public Map() {
        this.mappaDiGioco = new Tile[5][5];
    }

    public Tile[][] getMappaDiGioco() {
        return mappaDiGioco;
    }

    public void setMappaDiGioco(Tile[][] mappaDiGioco) {
        this.mappaDiGioco = mappaDiGioco;
    }
}
