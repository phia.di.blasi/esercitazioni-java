import java.util.ArrayList;
import java.util.List;

public class Studente extends Persona {
    private int annoImmatricolazione;
    private List<Corso> listaCorsi = new ArrayList<>();

    public Studente(String nome, String cognome, int id, int annoImmatricolazione, List<Corso> listaCorsi) {
        super(nome, cognome, id);
        this.annoImmatricolazione = annoImmatricolazione;
        this.listaCorsi = listaCorsi;
    }

    public int getAnnoImmatricolazione() {
        return annoImmatricolazione;
    }

    public List<Corso> getListaCorsi() {
        return listaCorsi;
    }

    public void setAnnoImmatricolazione(int annoImmatricolazione) {
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public void setListaCorsi(List<Corso> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }

    public void  addCorso(Corso nuovoCorso) throws IllegalArgumentException{
        if (this.getListaCorsi().contains(nuovoCorso)){
            throw new IllegalArgumentException("Sei già iscritto qua !");
        }
        for(Corso corso : this.getListaCorsi()){
            if(!corso.controllaSovraposizioneCorso(nuovoCorso)){
                throw new IllegalArgumentException("Hai già il corso: " + corso.getTitoloCorso() + " a quest'ora!" );
            }
        }
        this.listaCorsi.add(nuovoCorso);
    }
   public void rimuoviCorso (Corso corsoDaRimuovere){
      this.listaCorsi.remove(corsoDaRimuovere);
    }
    public void vediVoti(Corso corso) {
        System.out.println(corso.getMappaVoti().get(this));
    }
}
