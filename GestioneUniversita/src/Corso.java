import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Corso {
    private String titoloCorso;
    private int codiceCorso;
    private Professore professore;
    private int numeroMaxStudenti;
    private LocalTime orarioInizioCorso;
    private LocalTime orarioFineCorso;
    private List<Studente> listaStudenti = new ArrayList<>();
    private Map<Studente,Integer> mappaVoti = new HashMap<>();

    public Corso(String titoloCorso, int codiceCorso, Professore professore, int numeroMaxStudenti, LocalTime orarioInizioCorso, LocalTime orarioFineCorso, List<Studente> listaStudenti) {
        this.titoloCorso = titoloCorso;
        this.codiceCorso = codiceCorso;
        this.professore = professore;
        this.numeroMaxStudenti = numeroMaxStudenti;
        this.orarioInizioCorso = orarioInizioCorso;
        this.orarioFineCorso = orarioFineCorso;
        this.listaStudenti = listaStudenti;
    }

    public String getTitoloCorso() {
        return titoloCorso;
    }

    public int getCodiceCorso() {
        return codiceCorso;
    }

    public Professore getProfessore() {
        return professore;
    }

    public int getNumeroMaxStudenti() {
        return numeroMaxStudenti;
    }

    public List<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public LocalTime getOrarioInizioCorso() {
        return orarioInizioCorso;
    }

    public LocalTime getOrarioFineCorso() {
        return orarioFineCorso;
    }

    public Map<Studente, Integer> getMappaVoti() {
        return mappaVoti;
    }

    public void setTitoloCorso(String titoloCorso) {
        this.titoloCorso = titoloCorso;
    }

    public void setCodiceCorso(int codiceCorso) {
        this.codiceCorso = codiceCorso;
    }

    public void setProfessore(Professore professore) {
        this.professore = professore;
    }

    public void setNumeroMaxStudenti(int numeroMaxStudenti) {
        this.numeroMaxStudenti = numeroMaxStudenti;
    }
    public void setListaStudenti(List<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public void setOrarioInizioCorso(LocalTime orarioInizioCorso) {
        this.orarioInizioCorso = orarioInizioCorso;
    }

    public void setOrarioFineCorso(LocalTime orarioFineCorso) {
        this.orarioFineCorso = orarioFineCorso;
    }

    public void addStudente (Studente nuovoStudente) throws ArrayStoreException{
        if(this.listaStudenti.size() == this.numeroMaxStudenti){
            throw new ArrayStoreException("Oh no ci sono già tanti studenti, tu non puoi fare il corso!");
        } else {
            this.listaStudenti.add(nuovoStudente);
        }
    }
    public void rimuoviStudente(Studente studenteDaRimuovere){
       this.listaStudenti.remove(studenteDaRimuovere);
    }
    public boolean controllaSovraposizioneCorso(Corso corso){
        return this.orarioInizioCorso.isAfter(corso.getOrarioFineCorso()) || this.orarioFineCorso.isBefore(corso.getOrarioInizioCorso());
    }
}
