import java.util.ArrayList;
import java.util.List;

public class Universita {
    private List<Professore> listaProfessori = new ArrayList<>();
    private List<Studente> listaStudenti = new ArrayList<>();
    private List<Corso> listaCorsi= new ArrayList<>();

    public Universita(List<Professore> listaProfessori, List<Studente> listaStudenti, List<Corso> listaCorsi) {
        this.listaProfessori = listaProfessori;
        this.listaStudenti = listaStudenti;
        this.listaCorsi = listaCorsi;
    }

    public List<Professore> getListaProfessori() {
        return listaProfessori;
    }

    public List<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public List<Corso> getListaCorsi() {
        return listaCorsi;
    }

    public void setListaProfessori(List<Professore> listaProfessori) {
        this.listaProfessori = listaProfessori;
    }

    public void setListaStudenti(List<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public void setListaCorsi(List<Corso> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }

    public void addStudente (Studente nuovoStudente){
        this.listaStudenti.add(nuovoStudente);
    }
    public void addProfessore (Professore nuovoProfessore){
        this.listaProfessori.add(nuovoProfessore);
    }
    public void addCorso (Corso nuovoCorso){
        this.listaCorsi.add(nuovoCorso);
    }
   public void iscrizioneStudenteCorso(Studente nuovaIscrizione, Corso corsoScelto){
        corsoScelto.addStudente(nuovaIscrizione);
   }
   public void assegnaProf (Professore professore,Corso scorsoAss){
        scorsoAss.setProfessore(professore);
   }

   public void stampaStudenti(Corso corso){
        for (Studente studente : corso.getListaStudenti()){
            System.out.println(studente.getNome() + " " + studente.getCognome());
        }
   }
   public void elencoCorsiProf(Professore professore){
        for (Corso corso : professore.getCorsiCheInsegna()){
            System.out.println(corso.getCodiceCorso());
        }
   }
   public Studente cercaStudentePerMatricola(int idMatricola){
       for (Studente studente : this.listaStudenti) {
           if (studente.getId() == idMatricola) {
               return studente;
           }
       }
       return null;
   }
   public Professore cercaProfPerMatricola(int idProfessore){
       for (Professore professore : this.listaProfessori) {
           if (professore.getId() == idProfessore) {
               return professore;
           }
       }
       return null;
   }
   public Corso cercaCorso(int codiceCorso){
       for (Corso corso : this.listaCorsi) {
           if (corso.getCodiceCorso() == codiceCorso) {
               return corso;
           }
       }
       return null;
   }
}
