import java.util.ArrayList;
import java.util.List;

public class Professore extends Persona{
    private List<Corso> corsiCheInsegna = new ArrayList<>();

    public Professore(String nome, String cognome, int id, List<Corso> corsiCheInsegna) {
        super(nome, cognome, id);
        this.corsiCheInsegna = corsiCheInsegna;
    }

    public List<Corso> getCorsiCheInsegna() {
        return corsiCheInsegna;
    }

    public void setCorsiCheInsegna(List<Corso> corsiCheInsegna) {
        this.corsiCheInsegna = corsiCheInsegna;
    }
    public void addCorso(Corso nuovoCorso){
        this.corsiCheInsegna.add(nuovoCorso);
    }

    public void rimuoviCorso(Corso corsoDaRimuovere){
        this.corsiCheInsegna.remove(corsoDaRimuovere);
    }
    public void assegnaVoti(Corso corso, Studente studente, int voto){
        corso.getMappaVoti().put(studente,voto);
    }
}
