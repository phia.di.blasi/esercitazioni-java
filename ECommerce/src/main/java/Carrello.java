import java.util.HashMap;
import java.util.Map;

public class Carrello {
    private HashMap<Prodotto,Integer> listaQuantitaProdotti = new HashMap<>();
    private Promozione promo;

    public Carrello() {
    }

    //////METODI GET//////
    public HashMap<Prodotto, Integer> getListaQuantitaProdotti() {
        return listaQuantitaProdotti;
    }

    public Promozione getPromo() {
        return promo;
    }
    //////METODI SET//////

    public void setListaQuantitaProdotti(HashMap<Prodotto, Integer> listaQuantitaProdotti) {
        this.listaQuantitaProdotti = listaQuantitaProdotti;
    }

    public void setPromo(Promozione promo) {
        this.promo = promo;
    }
    public void aggiungiProdotto(Prodotto prodottoDaAggiungere, int quantitaProdotto){
        this.listaQuantitaProdotti.put(prodottoDaAggiungere,quantitaProdotto);
    }
    public void rimuoviProdotto(Prodotto prodottoDaRimuovere){
        this.listaQuantitaProdotti.remove(prodottoDaRimuovere);
    }
    public double calcolaTotaleNoPromo(){
        double totaleOrdine = 0;
        for (Map.Entry<Prodotto,Integer> entry : this.listaQuantitaProdotti.entrySet()){
            totaleOrdine = totaleOrdine + (entry.getKey().getPrezzoProdotto() * entry.getValue());
        }
        return totaleOrdine;
    }
    public double calcolaTotaleConPromo(){
        double totale = calcolaTotaleNoPromo();
        return totale * (1 - (this.promo.getPercentualeSconto() / 100));
    }
}
