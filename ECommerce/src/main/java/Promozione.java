public class Promozione {
    private String codicePromo;
    private String descrizionePromo;
    private double percentualeSconto;

    public Promozione(String codicePromo, String descrizionePromo, double percentualeSconto) {
        this.codicePromo = codicePromo;
        this.descrizionePromo = descrizionePromo;
        this.percentualeSconto = percentualeSconto;
    }
    //////METODI GET//////

    public String getCodicePromo() {
        return codicePromo;
    }

    public String getDescrizionePromo() {
        return descrizionePromo;
    }

    public double getPercentualeSconto() {
        return percentualeSconto;
    }
    //////METODI SET//////

    public void setCodicePromo(String codicePromo) {
        this.codicePromo = codicePromo;
    }

    public void setDescrizionePromo(String descrizionePromo) {
        this.descrizionePromo = descrizionePromo;
    }

    public void setPercentualeSconto(double percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }
    public void applicaSconto(Ordine ordineDaScontare){
        ordineDaScontare.calcoloTotaleOrdine();
        ordineDaScontare.setTotaleOrdine(ordineDaScontare.getTotaleOrdine() * (1 - (this.percentualeSconto / 100)));
    }
}
