import java.util.HashMap;

public class GestioneInventario {
    private HashMap<Prodotto,Integer> listaQuantitaProdotti = new HashMap<>();

    public GestioneInventario() {
    }
    //////METODI GET//////

    public HashMap<Prodotto, Integer> getListaQuantitaProdotti() {
        return listaQuantitaProdotti;
    }
    //////METODI SET//////

    public void setListaQuantitaProdotti(HashMap<Prodotto, Integer> listaQuantitaProdotti) {
        this.listaQuantitaProdotti = listaQuantitaProdotti;
    }
    public void aggiornaQuantitaProdotto(Prodotto prodotto, int quantitaDaTogliere){
        Integer quantitaProdotto = this.listaQuantitaProdotti.get(prodotto);
        this.listaQuantitaProdotti.put(prodotto,quantitaProdotto - quantitaDaTogliere);
    }
    public boolean verificaDisponibilita(Prodotto prodotto){
        Integer quantitaProdotto = this.listaQuantitaProdotti.get(prodotto);
        if (quantitaProdotto == null){
            quantitaProdotto = 0;
        }
        return quantitaProdotto > 0;
    }
    public void gestioneOrdiniRifornimento(Prodotto prodotto, int quantitaDaAggiungere){
        Integer quantitaProdotto = this.listaQuantitaProdotti.get(prodotto);
        if (quantitaProdotto == null){
            quantitaProdotto = 0;
        }
        this.listaQuantitaProdotti.put(prodotto,quantitaProdotto + quantitaDaAggiungere);
    }
}
