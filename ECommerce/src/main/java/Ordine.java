import java.util.HashMap;
import java.util.Map;

public class Ordine {
    private String idOrdine;
    private HashMap<Prodotto,Integer> listaQuantitaProdotti = new HashMap<>();
    private StatoOrdine statoOrdine;
    private double totaleOrdine;

    public Ordine(String idOrdine, StatoOrdine statoOrdine) {
        this.idOrdine = idOrdine;
        this.statoOrdine = statoOrdine;
    }

    //////METODI GET//////

    public String getIdOrdine() {
        return idOrdine;
    }

    public HashMap<Prodotto, Integer> getListaQuantitaProdotti() {
        return listaQuantitaProdotti;
    }

    public StatoOrdine getStatoOrdine() {
        return statoOrdine;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }
    //////METODI SET//////

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public void setListaQuantitaProdotti(HashMap<Prodotto, Integer> listaQuantitaProdotti) {
        this.listaQuantitaProdotti = listaQuantitaProdotti;
    }

    public void setStatoOrdine(StatoOrdine statoOrdine) {
        this.statoOrdine = statoOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = totaleOrdine;
    }
    public void aggiungiProdotto(Prodotto prodottoDaAggiungere, int quantitaProdotto){
        this.listaQuantitaProdotti.put(prodottoDaAggiungere,quantitaProdotto);
    }
    public void rimuoviProdotto(Prodotto prodottoDaRimuovere){
        this.listaQuantitaProdotti.remove(prodottoDaRimuovere);
    }
    public void calcoloTotaleOrdine(){
        double totaleOrdine = 0;
        for (Map.Entry<Prodotto,Integer> entry : this.listaQuantitaProdotti.entrySet()){
            totaleOrdine = totaleOrdine + (entry.getKey().getPrezzoProdotto() * entry.getValue());
        }
        this.totaleOrdine = totaleOrdine;
    }
}
