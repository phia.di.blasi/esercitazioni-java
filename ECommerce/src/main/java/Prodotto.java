public class Prodotto {
    private String idProdotto;
    private String nomeProdotto;
    private double prezzoProdotto;
    private int quantitaInventario;

    public Prodotto(String idProdotto, String nomeProdotto, double prezzoProdotto, int quantitaInventario) {
        this.idProdotto = idProdotto;
        this.nomeProdotto = nomeProdotto;
        this.prezzoProdotto = prezzoProdotto;
        this.quantitaInventario = quantitaInventario;
    }
    //////METODI GET//////
    public String getIdProdotto() {
        return idProdotto;
    }

    public String getNomeProdotto() {
        return nomeProdotto;
    }

    public double getPrezzoProdotto() {
        return prezzoProdotto;
    }

    public int getQuantitaInventario() {
        return quantitaInventario;
    }

    //////METODI SET//////
    public void setIdProdotto(String idProdotto) {
        this.idProdotto = idProdotto;
    }

    public void setNomeProdotto(String nomeProdotto) {
        this.nomeProdotto = nomeProdotto;
    }

    public void setPrezzoProdotto(double prezzoProdotto) {
        this.prezzoProdotto = prezzoProdotto;
    }

    public void setQuantitaInventario(int quantitaInventario) {
        this.quantitaInventario = quantitaInventario;
    }
    public boolean disponibilitaProdottoInInventario(){
       return this.quantitaInventario > 0;
    }
}
