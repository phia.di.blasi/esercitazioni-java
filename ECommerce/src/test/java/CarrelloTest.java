import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarrelloTest {
    static Carrello carrello;
    static Prodotto prodotto;
    static Promozione promo;


    @BeforeAll
    static void setUp() {
        carrello = new Carrello();
        prodotto = new Prodotto("1", "prodotto1", 10, 10);
        promo = new Promozione("1", "promo1", 10);
    }

    @Test
    void aggiungiRimuoviProdotto() {
        assertEquals(0, carrello.getListaQuantitaProdotti().entrySet().size());
        carrello.aggiungiProdotto(prodotto, 10);
        assertEquals(10, carrello.getListaQuantitaProdotti().get(prodotto));
        carrello.rimuoviProdotto(prodotto);
        assertEquals(0, carrello.getListaQuantitaProdotti().entrySet().size());
    }

    @Test
    void testVerificaPromo(){
        carrello.aggiungiProdotto(prodotto, 10);
        assertEquals(100, carrello.calcolaTotaleNoPromo());
        carrello.setPromo(promo);
        assertEquals(90, carrello.calcolaTotaleConPromo());
    }


}