import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestioneInventarioTest {
    static GestioneInventario inventario;
    static Prodotto prodotto;

    @BeforeAll
    static void setUp() {
        inventario = new GestioneInventario();
        prodotto = new Prodotto("1", "prodotto1", 10, 10);
    }

    @Test
    void testAggiornaQuantitaProdotto() {
        assertEquals(0, inventario.getListaQuantitaProdotti().entrySet().size());
        inventario.gestioneOrdiniRifornimento(prodotto, 10);
        assertEquals(10, inventario.getListaQuantitaProdotti().get(prodotto));
        inventario.aggiornaQuantitaProdotto(prodotto, 10);
        assertEquals(0, inventario.getListaQuantitaProdotti().get(prodotto));
    }

    @Test
    void testVerificaDisponibilita() {
        assertEquals(false, inventario.verificaDisponibilita(prodotto));
        inventario.gestioneOrdiniRifornimento(prodotto, 10);
        assertEquals(true, inventario.verificaDisponibilita(prodotto));
    }
}