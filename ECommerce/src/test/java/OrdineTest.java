import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrdineTest {
    static Ordine ordine;
    static Prodotto prodotto;

    @BeforeAll
    static void setUp() {
        ordine = new Ordine("1", StatoOrdine.IN_ATTESA);
        prodotto = new Prodotto("1", "prodotto1", 10, 10);
    }

    @Test
    void testVerificaTotaleOrdine(){
        double valoreAtteso = 0;
        ordine.calcoloTotaleOrdine();
        assertEquals(valoreAtteso, ordine.getTotaleOrdine());
        ordine.aggiungiProdotto(prodotto, 1);
        ordine.calcoloTotaleOrdine();
        assertEquals(10, ordine.getTotaleOrdine());
        ordine.rimuoviProdotto(prodotto);
        ordine.calcoloTotaleOrdine();
        assertEquals(0, ordine.getTotaleOrdine());
    }

    @Test
    void testVerificaStatoOrdine(){
        assertEquals(StatoOrdine.IN_ATTESA, ordine.getStatoOrdine());
        ordine.setStatoOrdine(StatoOrdine.CONSEGNATO);
        assertEquals(StatoOrdine.CONSEGNATO, ordine.getStatoOrdine());
    }


}