import java.util.ArrayList;

public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private Accessorio[] accessori;
    private int numAccessori;
    private double prezzo;

    ///COSTRUTTORE///
    public VeicoloBase(String tipoVeicolo, int potenzaMotore, int numAccessori, double prezzo) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.numAccessori = numAccessori;
        this.prezzo = prezzo;
        this.accessori = new Accessorio[numAccessori];
    }
    ///GET///
    public String getTipoVeicolo() {
        return tipoVeicolo;
    }

    public int getPotenzaMotore() {
        return potenzaMotore;
    }

    public Accessorio[] getAccessori() {
        return accessori;
    }

    public int getNumAccessori() {
        return numAccessori;
    }

    public double getPrezzo() {
        return prezzo;
    }
    ///SET///
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }

    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }

    public void setAccessori(Accessorio[] accessori) {
        this.accessori = accessori;
    }

    public void setNumAccessori(int numAccessori) {
        this.numAccessori = numAccessori;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
    ///ALTRI METODI///

    /**
     * Questo metodo si occupa di aggiungere un nuovo Accessorio al veicolo.
     *
     * @param nuovoAccessorio è il nuovo accessorio da aggiungere
     */
    public void aggiungiAccessorio(Accessorio nuovoAccessorio){
        boolean accessorioAggiunto = false;
        for (int i = 0; i < this.accessori.length ; i++) {
            if (this.accessori[i] == null){
                this.accessori[i] = nuovoAccessorio;
                accessorioAggiunto = true;
                break;
            }
        }
        if (accessorioAggiunto){
            System.out.println("Accessorio correttamente aggiunto");
        } else {
            System.out.println("Non puoi aggiungere accessori");
        }
    }
    /**
     *Questo metodo si occupa di creare una copia dell'array Accessorio.
     *
     * @return ritorna una copia della lista Accessorio
     */
    public Accessorio[] copiaArrayAccessorio(){
        return  this.accessori.clone();
    }
    /**
     *Questo metodo si occupa di calcolare la velocità massima del nostro veicolo.
     *
     * @return ritorna la velocità massima del veicolo
     */
    public double calcolaVelocitaMassima(){
        return this.potenzaMotore * 4;
    }
    /**
     * Questo metodo si occupa di stampare le info dell'accessorio.
     *
     */
    public void stampaAccessori(){
        for (int i = 0; i < this.accessori.length; i++) {
           Accessorio accessorioIndex = this.accessori[i];
            System.out.println("Il nome dell'accessorio scelto è "
                    + accessorioIndex.getNome()
                    + " ed il suo prezzo è di "
                    + accessorioIndex.getPrezzo());
        }
    }
    /**
     * Questo metodo si occupa di calcolare il costo totale del veicolo compreso di accessori.
     *
     * @return il costo totale del veicolo compreso di accessori
     */
    public double calcolaCostoTotale(){
        double totale = this.prezzo;
        for (int i = 0; i < this.accessori.length; i++) {
            Accessorio accessorioIndex = this.accessori[i];
            if (accessorioIndex != null){
                totale = totale + accessorioIndex.getPrezzo();
            }
        }
        return  totale;
    }
    /**
     * Questo metodo si occupa di capire il consumo di carburante in una simulazione di viaggio.
     *
     * @param distanza è la distanza da percorrere.
     * @param consumoMedio è il consumo medio.
     */
    public void simulaViaggio(double distanza,double consumoMedio){
        System.out.println((distanza / 100) / consumoMedio);
    }
    /**
     * Questo metodo calcola la velocità media del veicolo.
     *
     * @return la velocità media del veicolo
     */
    public double calcolaVelocitaMedia(){
        return Math.random() * 100;
    }
}
