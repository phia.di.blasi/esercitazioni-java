import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VeicoloBaseTest {
    static VeicoloBase veicolo;
    static Accessorio accessorio;

    /**
     *Test setUp
     */
    @BeforeAll
    static void setUp(){
        veicolo = new VeicoloBase("auto",27,2, 20450);
        accessorio = new Accessorio("sensore di parcheggio", 1200);
    }

    /**
     * Test per calcolare la velocità massima del veicolo
     */
    @Test
    void testCalcolaVelocitaMassima(){
        assertEquals(108,veicolo.calcolaVelocitaMassima());
    }
    /**
     * Test per calcolare il prezzo totale, prima senza e poi con accessorio dopo averlo aggiunto
     */
    @Test
    void testCalcolaCostoTotale(){
        assertEquals(veicolo.getPrezzo(),veicolo.calcolaCostoTotale());
        veicolo.aggiungiAccessorio(accessorio);
        assertEquals(21650.0,veicolo.calcolaCostoTotale());
    }
}