import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RisorsaEnergeticaTest {
    static RisorsaEnergetica risorsa;
    /**
     * Inizializza le variabili per i Test
     */
    @BeforeAll
    static void setUp(){
        risorsa = new RisorsaEnergetica(NomeRisorsa.GAS_NATURALE, 200, 2, 10);
    }

    /**
     * Testa il calcolo del costo di una quantità specifica di risorsa energetica.
     */
    @Test
    public void testCalcolaCosto() {
        double quantita = 10.0;
        double costoAtteso = 10 * 10;
        assertEquals(costoAtteso, risorsa.calcolaCosto(quantita));
    }
}