import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CentraleElettricaTest {
    static CentraleElettrica centrale;
    static RisorsaEnergetica nuovaRisorsa;
    /**
     * Inizializza le variabili per i Test
     */
    @BeforeAll
    static void setUp() {
        centrale = new CentraleElettrica(new ArrayList<>(), 0.9);
        nuovaRisorsa = new RisorsaEnergetica(NomeRisorsa.GAS_NATURALE, 200, 2, 10);

    }
    /**
     * Testa il metodo di aggiunta di una risorsa alla centrale elettrica.
     */
    @Test
    public void testAggiungiRisorsa() {
        int grandezzaIniziale = centrale.getRisorseDisponibili().size();
        centrale.aggiungiRisorsa(nuovaRisorsa);
        assertEquals(grandezzaIniziale + 1, centrale.getRisorseDisponibili().size());
    }
    /**
     * Testa il calcolo della produzione energetica totale della centrale elettrica.
     */
    @Test
    public void testCalcolaProduzioneEnergetica() {
        centrale.aggiungiRisorsa(nuovaRisorsa);
        double produzioneAttesa = nuovaRisorsa.getQuantitaDisponibile() * nuovaRisorsa.getPotenzaCalorifera() * centrale.getEfficienza();
        assertEquals(produzioneAttesa, centrale.calcolaProduzioneEnergetica());
    }
}