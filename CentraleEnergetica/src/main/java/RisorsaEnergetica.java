/**
 * Classe che rappresenta una risorsa energetica con attributi come nome, quantità disponibile, potenza calorifera e prezzo.
 */
public class RisorsaEnergetica {
    //attributi di classe
    private NomeRisorsa nome;
    private double quantitaDisponibile;
    private double potenzaCalorifera;
    private double prezzo;

    //costruttore

    /**
     * Costruttore della classe
     *
     * @param nome è il nome della risorsa energetica
     * @param quantitaDisponibile è la quantità di risorsa disponibile
     * @param potenzaCalorifera è la potenza calorifera della risorsa energetica
     * @param prezzo è il prezzo della risorsa
     */
    public RisorsaEnergetica(NomeRisorsa nome, double quantitaDisponibile, double potenzaCalorifera, double prezzo) {

        this.nome = nome;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifera = potenzaCalorifera;
        this.prezzo = prezzo;
    }
    //GET//

    public NomeRisorsa getNome() {
        return nome;
    }

    public double getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    public double getPotenzaCalorifera() {
        return potenzaCalorifera;
    }

    public double getPrezzo() {
        return prezzo;
    }
    //SET//

    public void setNome(NomeRisorsa nome) {
        this.nome = nome;
    }

    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public void setPotenzaCalorifera(double potenzaCalorifera) {
        this.potenzaCalorifera = potenzaCalorifera;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
    //ALTRI METODI//

    /**
     * Calcola il costo moltiplicando la quantità per il prezzo.
     *
     * @param  quantita la quantità da utilizzare nel calcolo
     * @return il costo calcolato
     */
    public double calcolaCosto(double quantita){
        return quantita * this.prezzo;
    }
}
