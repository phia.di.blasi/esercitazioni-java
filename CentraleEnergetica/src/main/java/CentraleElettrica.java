import java.util.ArrayList;
import java.util.List;

public class CentraleElettrica {
    //attributi di classe
    private List<RisorsaEnergetica> risorseDisponibili = new ArrayList<>();
    private int numeroRisorse = risorseDisponibili.size();
    private double efficienza;

    //costruttore

    /**
     * Costruttore della classe
     * @param risorseDisponibili è la lista di risorse
     * @param efficienza è l'efficienza della centrale
     */
    public CentraleElettrica(List<RisorsaEnergetica> risorseDisponibili, double efficienza) {
        this.risorseDisponibili = risorseDisponibili;
        this.efficienza = efficienza;
    }

    //get

    public List<RisorsaEnergetica> getRisorseDisponibili() {
        return risorseDisponibili;
    }

    public int getNumeroRisorse() {
        return numeroRisorse;
    }

    public double getEfficienza() {
        return efficienza;
    }
    //set

    public void setRisorseDisponibili(List<RisorsaEnergetica> risorseDisponibili) {
        this.risorseDisponibili = risorseDisponibili;
    }

    public void setEfficienza(double efficienza) {
        this.efficienza = efficienza;
    }
    //altri metodi

    /**
     * Aggiunge una risorsa alla centrale elettrica.
     *
     * @param nuovaRisorsa La risorsa da aggiungere.
     */
    public void aggiungiRisorsa(RisorsaEnergetica nuovaRisorsa){
        this.risorseDisponibili.add(nuovaRisorsa);
    }
    /**
     * Visualizza le risorse disponibili nella centrale elettrica.
     *
     */
    public void visualizzaRisorse(){
        for (RisorsaEnergetica risorsa : this.risorseDisponibili) {
            System.out.println(risorsa.getNome()
                    + " "
                    + risorsa.getQuantitaDisponibile()
                    + " "
                    + risorsa.getPotenzaCalorifera()
                    + " "
                    + risorsa.getPrezzo());
        }
    }
    /**
     * Calcola la produzione energetica totale considerando la quantità disponibile delle risorse e l'efficienza della centrale.
     *
     * @return La produzione energetica totale.
     */
    public double calcolaProduzioneEnergetica(){
        double totale = 0;
        for (RisorsaEnergetica risorsa : this.risorseDisponibili){
            totale = totale + (risorsa.getQuantitaDisponibile() * risorsa.getPotenzaCalorifera());
        }
        return totale * this.efficienza;
    }
    /**
     * Simula il consumo energetico della centrale elettrica.
     *
     * @param consumo Il consumo energetico da simulare.
     */
    public void simulaConsumo(double consumo){
        for (RisorsaEnergetica risorsa : this.risorseDisponibili){
            double consumoRisorsa = consumo / risorsa.getPotenzaCalorifera();
            if (consumoRisorsa > risorsa.getQuantitaDisponibile()){
                System.out.println("Non ho abbastanza risorse per il: " + risorsa.getNome()
                + " perchè ho: " + risorsa.getQuantitaDisponibile()
                + " e dovrei consumare "
                + consumoRisorsa);
            } else {
                risorsa.setQuantitaDisponibile(risorsa.getQuantitaDisponibile() - (consumo / risorsa.getPotenzaCalorifera()));
            }
        }
    }
    /**
     * Calcola il costo dell'energia consumata considerando le risorse disponibili nella centrale elettrica.
     *
     * @param consumo Il consumo energetico per il quale calcolare il costo.
     * @return Il costo dell'energia consumata.
     */
    public double calcolaCostoEnergia(double consumo){
        double totale = 0;
        for (RisorsaEnergetica risorsa : this.risorseDisponibili){
            totale = totale + (consumo / risorsa.getPotenzaCalorifera()) * risorsa.getPrezzo();
        }
        return totale;
    }
}
