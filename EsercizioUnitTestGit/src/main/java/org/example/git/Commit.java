package org.example.git;

public class Commit {
    private String id;
    private String messaggio;

    @Override
    public String toString() {
        return "Commit: " +
                "id = " + id +
                ", messaggio = " + messaggio;
    }

    public Commit(String id, String messaggio) {
        this.id = id;
        this.messaggio = messaggio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }
}
