package org.example.git;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    private String nome;
    private List<Commit> commits;


    public Repository() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Commit> getCommits() {
        return commits;
    }

    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }

    public void aggiungiCommit(Commit commit){
        this.commits.add(commit);
    }

    public Commit ultimoCommit(){
        if(commits.isEmpty()){
            return null;
        } else {
            int indexLastCommit = commits.size() - 1;
            return commits.get(indexLastCommit);
        }
    }

    public int numeroDiCommits(){
        return commits.size();
    }

}
