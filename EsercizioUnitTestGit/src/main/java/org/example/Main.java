package org.example;

import org.example.git.Commit;
import org.example.git.Repository;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        Commit commit = new Commit("1abc", "primo commit");
        Commit commit2 = new Commit("2abc", "secondo commit");
        Commit commit3 = new Commit("3abc", "terzo commit");

        Repository repo = new Repository();

        repo.aggiungiCommit(commit);
        repo.aggiungiCommit(commit2);
        repo.aggiungiCommit(commit3);

        System.out.println(repo.ultimoCommit());

        System.out.println(repo.numeroDiCommits());
    }
}