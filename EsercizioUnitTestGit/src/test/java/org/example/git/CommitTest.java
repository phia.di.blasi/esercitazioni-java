package org.example.git;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommitTest {
    static Commit commit;

    @BeforeAll
    static void setUp(){
        commit = new Commit("123C","Questa è una commit");
    }
    @Test void testCommit(){
        assertEquals("123C",commit.getId());
        assertEquals("Questa è una commit",commit.getMessaggio());
    }
}