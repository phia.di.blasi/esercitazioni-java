package org.example.git;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {
    static Repository repository;
    static Commit commit;

    @BeforeAll
    static void setUp(){
        repository = new Repository();
        commit = new Commit("123C","Questa è una commit");
    }
    @Test
    void aggiungiCommit(){
        assertEquals(0,repository.getCommits().size());
        repository.aggiungiCommit(commit);
        assertEquals(1,repository.getCommits().size());
    }
}