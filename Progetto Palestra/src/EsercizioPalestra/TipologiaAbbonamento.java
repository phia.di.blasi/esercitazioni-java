package EsercizioPalestra;

public enum TipologiaAbbonamento {
    GIORNALIERO,
    MENSILE,
    ANNUALE
}
