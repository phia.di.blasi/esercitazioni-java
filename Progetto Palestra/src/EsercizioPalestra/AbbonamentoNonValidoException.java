package EsercizioPalestra;

public class AbbonamentoNonValidoException extends RuntimeException{
    /**
     * L'eccezione viene lanciata quando l'abbonamento del membro non è più valido.
     */
    public AbbonamentoNonValidoException() {
        super();
    }
}
