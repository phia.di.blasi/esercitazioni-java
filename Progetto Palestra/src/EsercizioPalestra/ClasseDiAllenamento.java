package EsercizioPalestra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClasseDiAllenamento {
    //Variabili di istanza
    private String nomeClasse;
    private String orarioClasse;
    private String giornoClasse;
    private List<Membro> listaMembri;
    private Istruttore istruttore;
    private int numeroMaxPartecipanti;
    private Map<String,Integer> voti;

    /**
     * E' il costruttore che inizializza lista membri e mappa voti
     *
     * @param nomeClasse è il nome della classe
     * @param orarioClasse è l'orario a cui si tiene la classe
     * @param giornoClasse è il giorno della settimana (es. Lunedì) in cui si tiene la classe
     * @param istruttore è l'istruttore della classe
     * @param numeroMaxPartecipanti è il numeor masismo di partcipanti ad una classe
     */
    //Costruttore di classe
    //All'interno del primo costruttore non incluso direttamente la lista dei membri, ma la inizializzo così
    //posso poi aggiungerli al corso in un secondo momento
    public ClasseDiAllenamento(String nomeClasse, String orarioClasse, String giornoClasse, Istruttore istruttore, int numeroMaxPartecipanti) {
        this.nomeClasse = nomeClasse;
        this.orarioClasse = orarioClasse;
        this.giornoClasse = giornoClasse;
        this.istruttore = istruttore;
        this.numeroMaxPartecipanti = numeroMaxPartecipanti;
        this.listaMembri = new ArrayList<>();
        this.voti = new HashMap<>();
    }
    /**
     * E' il costruttore completo
     *
     * @param nomeClasse è il nome della classe
     * @param orarioClasse è l'orario a cui si tiene la classe
     * @param giornoClasse è il giorno della settimana (es. Lunedì) in cui si tiene la classe
     * @param istruttore è l'istruttore della classe
     * @param numeroMaxPartecipanti è il numeor masismo di partcipanti ad una classe
     */
    public ClasseDiAllenamento(String nomeClasse, String orarioClasse, String giornoClasse, List<Membro> listaMembri, Istruttore istruttore, int numeroMaxPartecipanti,HashMap<String,Integer> voti) {
        this.nomeClasse = nomeClasse;
        this.orarioClasse = orarioClasse;
        this.giornoClasse = giornoClasse;
        this.listaMembri = listaMembri;
        this.istruttore = istruttore;
        this.numeroMaxPartecipanti = numeroMaxPartecipanti;
        this.voti = voti;
    }
    //Metodi Get
    public String getNomeClasse() {
        return nomeClasse;
    }

    public String getOrarioClasse() {
        return orarioClasse;
    }

    public List<Membro> getListaMembri() {
        return listaMembri;
    }

    public Istruttore getIstruttore() {
        return istruttore;
    }
    public int getNumeroMaxPartecipanti() {
        return numeroMaxPartecipanti;
    }
    public String getGiornoClasse() {
        return giornoClasse;
    }

    public Map<String, Integer> getVoti() {
        return voti;
    }

    //Metodi Set
    public void setNomeClasse(String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }

    public void setOrarioClasse(String orarioClasse) {
        this.orarioClasse = orarioClasse;
    }

    public void setListaMembri(List<Membro> listaMembri) {
        this.listaMembri = listaMembri;
    }

    public void setIstruttore(Istruttore istruttore) {
        this.istruttore = istruttore;
    }

    public void setNumeroMaxPartecipanti(int numeroMaxPartecipanti) {
        this.numeroMaxPartecipanti = numeroMaxPartecipanti;
    }
    public void setGiornoClasse(String giornoClasse) {
        this.giornoClasse = giornoClasse;
    }
    public void setVoti(Map<String, Integer> voti) {
        this.voti = voti;
    }

    /**
     * Questo metodo permette di aggiungere un nuovo membro ad una classe, inoltre richiama il metodo per l'aggiornamento dello stato dell'abbonamento del singolo membro.
     *
     * @param nuovoMembro è il nuovo membro da aggiungere alla classe
     * @throws ClassePienaException viene lanciata quando la classe ha raggiunto il numero massismo di memmbri
     * @throws AbbonamentoNonValidoException viene lanciata quando l'abbonamento del membro non è più valido
     */
    public void aggiungiMembro (Membro nuovoMembro) throws ClassePienaException,AbbonamentoNonValidoException{
        //richiamo il metodo per l'aggiornamento dello stato dell'abbonamento del membro
        //sarò sicura che lo stato validità abbonamento sia aggiornato ad oggi.
        nuovoMembro.controlloStatoAbbonamento();

        //se l'abbonamento non è valido lancio l'eccezione
        if (!nuovoMembro.isAbbonamentoValido()){
            throw new AbbonamentoNonValidoException();
        }

        //controllo prima di aggiungerlo se effettivamente
        //c'è lo spazio o se la classe ha già il numero max di membri. Inoltre dichiaro che potrebbe
        //essere lanciata una eccezione nel caso in cui effettivamente la classe cui si vuole iscrivere
        // sia già piena.
        if(this.listaMembri.size() == this.numeroMaxPartecipanti){
            throw new ClassePienaException();
        }
        this.listaMembri.add(nuovoMembro);
    }
}
