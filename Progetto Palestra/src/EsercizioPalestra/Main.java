package EsercizioPalestra;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Membro membro1 = new Membro("Sofia","DiBlasi","SD11", TipologiaAbbonamento.MENSILE, LocalDate.of(2024,4,3), true);
        Membro membro2 = new Membro("Mario","Rossi","MR55", TipologiaAbbonamento.ANNUALE, LocalDate.of(2023,12,1), true);
        Membro membro3 = new Membro("Filippo","Bianchi","FB897", TipologiaAbbonamento.GIORNALIERO, LocalDate.of(2024,4,1), false);

        Istruttore istruttore1 = new Istruttore("Roberto","Verdi","RV134",null,null,true,"Pilates",5);
        Istruttore istruttore2 = new Istruttore("Giuseppe","Blu","GB412",null,null,true,"Ricomposizione corporea",15);

        ClasseDiAllenamento classe1 = new ClasseDiAllenamento("Pilates Base","12:00","Martedì",istruttore1,10);
        ClasseDiAllenamento classe2 = new ClasseDiAllenamento("Ginnastica","09:00","Venerdì",istruttore2,1);

        GestionePalestra palestra = new GestionePalestra();

        palestra.nuovaClasseAllenamento(classe1);
        palestra.nuovaClasseAllenamento(classe2);

        palestra.registrazioneNuovoMembro(membro1,membro1.getAbbonamento());
        palestra.registrazioneNuovoMembro(membro2,membro2.getAbbonamento());
        palestra.registrazioneNuovoMembro(membro3,membro3.getAbbonamento());

        palestra.assegnaInsegnante(istruttore2,classe1);

        palestra.iscrizioneMembroClasse(membro1,classe2);
        palestra.iscrizioneMembroClasse(membro2,classe2);
        palestra.iscrizioneMembroClasse(membro2,classe1);
        palestra.iscrizioneMembroClasse(membro3,classe2);

        palestra.dettagliCorsiGiornalieri(LocalDate.of(2024,6,11));
        palestra.dettagliCorsiGiornalieri(LocalDate.of(2024,10,18));

        palestra.votaIlCorso(membro1,classe2,3);
        palestra.votaIlCorso(membro2,classe2,1);
        palestra.votaIlCorso(membro2,classe1,5);

        System.out.println("Il tasso di gradimento del corso: " + classe1.getNomeClasse() + " è di: " + palestra.calcoloMediaCorso(classe1));
        System.out.println("Il tasso di gradimento del corso: " + classe2.getNomeClasse() + " è di: " + palestra.calcoloMediaCorso(classe2));

    }
}
