package EsercizioPalestra;

public class ClassePienaException extends RuntimeException {
    /**
     * L'eccezione viene lanciata quando ad una classe già al completo si tenta l'iscrizione di un nuovo membro.
     */
    public ClassePienaException() {
        super();
    }
}
