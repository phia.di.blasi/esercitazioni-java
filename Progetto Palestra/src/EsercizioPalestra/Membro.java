package EsercizioPalestra;

import java.time.LocalDate;

public class Membro {
    //Variabili di istanza
    private String nome;
    private String cognome;
    private String idMembro;
    private TipologiaAbbonamento abbonamento;
    private LocalDate dataIscrizione;
    private boolean abbonamentoValido;


    /**
     * E' il costruttore edl membro.
     *
     * @param nome è il nome del membro.
     * @param cognome è il cognome del membro.
     * @param idMembro è l'ID univoco legato al membro.
     * @param abbonamento è la tipologia di abbonamento scelta dall'utente(Giornaliero,Mensile o Annuale).
     * @param dataIscrizione è la data di iscrizione dalla quale parte la validità dell'abbonamento.
     * @param abbonamentoValido dichiara o meno la validità dell'abbonamento.
     */
    //Costruttore di classe
    public Membro(String nome, String cognome, String idMembro, TipologiaAbbonamento abbonamento,LocalDate dataIscrizione, boolean abbonamentoValido) {
        this.nome = nome;
        this.cognome = cognome;
        this.idMembro = idMembro;
        this.abbonamento = abbonamento;
        this.dataIscrizione = dataIscrizione;
        this.abbonamentoValido = abbonamentoValido;
    }

    //Metodi Get
    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public String getIdMembro() {
        return idMembro;
    }
    public TipologiaAbbonamento getAbbonamento() {
        return abbonamento;
    }
    public LocalDate getDataIscrizione() {
        return dataIscrizione;
    }
    public boolean isAbbonamentoValido() {
        return abbonamentoValido;
    }

    //Metodi Set
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public void setIdMembro(String idMembro) {
        this.idMembro = idMembro;
    }
    public void setAbbonamento(TipologiaAbbonamento abbonamento) {
        this.abbonamento = abbonamento;
    }
    public void setDataIscrizione(LocalDate dataIscrizione) {
        this.dataIscrizione = dataIscrizione;
    }
    public void setAbbonamentoValido(boolean abbonamentoValido) {
        this.abbonamentoValido = abbonamentoValido;
    }

    /**
     * Questo metodo controlla lo stato di validità deòò'abbonamento dell'utente.
     */
    public void controlloStatoAbbonamento(){
        //Inizializzo una variabile "dataDiScadenza" che sarà uguale ad oggi (con il .now).
        LocalDate  dataScadenta = LocalDate.now();
        //Uso lo switch\case per ricalcolare la data di scadenza in base al tipo di abbonamento
        switch (this.abbonamento){
            case GIORNALIERO :
                dataScadenta = this.dataIscrizione.plusDays(1);
                break;
            case MENSILE:
                dataScadenta = this.dataIscrizione.plusMonths(1);
                break;
            case ANNUALE:
                dataScadenta = this.dataIscrizione.plusYears(1);
                break;
        }
        //alla fine verifico la validità dell'abbonamento confrontando se oggi è prima della data
        //di scadenza, e se quindi sono ancora abbonata o meno.
        this.abbonamentoValido = LocalDate.now().isBefore(dataScadenta);
    }
}
