package EsercizioPalestra;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;

public class GestionePalestra {
    //Variabili di istanza
    private Map<String,TipologiaAbbonamento> mappaAbbonamentiMembri;
    private List<ClasseDiAllenamento> listaClassi;

    /**
     * Costruttore  che inizializza lista degli abbonamenti dei membri della palestra e lista classi.
     */
    //Costruttore di Classe
    //Anche qui lascio il primo costruttore vuoto e inizializzo map e list così
    //posso poi aggiungere corsi e membri in un secondo momento.
    public GestionePalestra() {
        this.mappaAbbonamentiMembri = new HashMap<>();
        this.listaClassi = new ArrayList<>();
    }
    /**
     * Costruttore della classe completo
     */
    public GestionePalestra(Map<String, TipologiaAbbonamento> mappaAbbonamentiMembri, List<ClasseDiAllenamento> listaClassi) {
        this.mappaAbbonamentiMembri = mappaAbbonamentiMembri;
        this.listaClassi = listaClassi;
    }

    //Metodi Get
    public Map<String, TipologiaAbbonamento> getMappaAbbonamentiMembri() {
        return mappaAbbonamentiMembri;
    }
    public List<ClasseDiAllenamento> getListaClassi() {
        return listaClassi;
    }
    //Metodi Set
    public void setMappaAbbonamentiMembri(Map<String, TipologiaAbbonamento> mappaAbbonamentiMembri) {
        this.mappaAbbonamentiMembri = mappaAbbonamentiMembri;
    }
    public void setListaClassi(List<ClasseDiAllenamento> listaClassi) {
        this.listaClassi = listaClassi;
    }
    /**
     * Questo metodo si occupa di registrare un nuovo membro con l'abbonamenteo abbinato.
     *
     * @param nuovoMembro è il nuovo membro da registrare.
     * @param abbonamentoScelto è la tipologia di abbonamento scelta dall'utente.
     */
    public void registrazioneNuovoMembro(Membro nuovoMembro,TipologiaAbbonamento abbonamentoScelto){
        this.mappaAbbonamentiMembri.put(nuovoMembro.getIdMembro(),abbonamentoScelto);
    }

    /**
     * Questo metodo si occupa di aggiungere una nuova classe alla lista di classi(di allenamento) già esistenti.
     *
     * @param nuovaClasse è la nuova classe che verrà aggiunta.
     */
    public void nuovaClasseAllenamento(ClasseDiAllenamento nuovaClasse){
        this.listaClassi.add(nuovaClasse);
    }

    /**
     *  Questo metodo si occupa di assegnare un istruttore ad una data classe.
     *
     * @param istruttoreDaAssegnare è l'istruttore che verrà assegnato ad una classe di allenamento.
     * @param classe è la classe di allenamento.
     */
    public void assegnaInsegnante(Istruttore istruttoreDaAssegnare, ClasseDiAllenamento classe){
        //Se la classe è già presente nella mia lista di corsi allora gli assegno semplicemente un istruttore
        if (this.listaClassi.contains(classe)){
            classe.setIstruttore(istruttoreDaAssegnare);
        } else {
            //altrimenti aggiungo una nuova classe alla mia lista di corsi disponibili in palestra
            //e poi gli assegno un istruttore
            this.nuovaClasseAllenamento(classe);
            classe.setIstruttore(istruttoreDaAssegnare);
        }
    }

    /**
     *  Questo metodo si occupa di iscrivere un membro ad una data classe e gestisce le eccezioni.
     *
     * @param membroDaIscrivere è il membro che si si andrà ad iscrivere ad una classe di allenamento.
     * @param classeScelta è la classe scelta dall'utente.
     */
    public void iscrizioneMembroClasse(Membro membroDaIscrivere,ClasseDiAllenamento classeScelta){
        //richiamamndo in questo metodo un altro metodo che potrebbe tirate più eccezzioni vado a gestirle
        try{
            classeScelta.aggiungiMembro(membroDaIscrivere);
        } catch (ClassePienaException e){
            System.out.println("Mi spiace la classe: " + classeScelta.getNomeClasse() +  " è già al completo.");
        } catch (AbbonamentoNonValidoException e){
            System.out.println("Mi spiace, ma il tuo abbonamento è scaduto.");
        }
    }

    /**
     *  Questo metodo si occupa di fornire tutti i dettagli di un dato corso di allenamento in base alla data che viene fornita.
     *
     * @param data è la data del corso.
     */
    public void dettagliCorsiGiornalieri (LocalDate data){
        //uso dayOfWeek per recuperare il giorno della settimana tramite la data che viene fornita
        DayOfWeek giorno = data.getDayOfWeek();
        //recuper il nome di quel giorno della settimana utilizzando il Loca.getDefault che userà la lingua del mio sistema
        String nomeGiorno = giorno.getDisplayName(TextStyle.FULL, Locale.getDefault());

        //ciclo tutti i corsi e stampo solo quelli che avranno come giorno quello = alla data passata sopra
        for (ClasseDiAllenamento classe : this.listaClassi){
            if(classe.getGiornoClasse().equalsIgnoreCase(nomeGiorno)){
                System.out.println("Il corso " + classe.getNomeClasse() + " ha come orario "
                + classe.getOrarioClasse() + " tutti i " + classe.getGiornoClasse() + " sarà tenuto dall'Istruttore "
                + classe.getIstruttore().getNome() + " " + classe.getIstruttore().getCognome()
                + " ed avrà come numero massimo di membri " + classe.getNumeroMaxPartecipanti());
            }
        }
    }

    /**
     *  Questo metodo si occupa di far votare un dato corso.
     *
     * @param membroVotante è il membro che vota il corso.
     * @param corsoDaVotare è il corso che viene votato.
     * @param voto è il voto dato.
     */
    public void votaIlCorso (Membro membroVotante,ClasseDiAllenamento corsoDaVotare,int voto){
        corsoDaVotare.getVoti().put(membroVotante.getIdMembro(),voto);
    }

    /**
     * Questo metodo si occupa di calcolare la media dei voti dati da più utenti ad una classe.
     *
     * @param classe è la classe di allenamento votata.
     * @return la media dei voti dati ad un corso.
     */
    public double calcoloMediaCorso(ClasseDiAllenamento classe){
        int sommaVoti = 0;
        //uso il ciclo for per sommare tutti i voti ricevuti dal corso dai vari utenti
        for (Map.Entry<String,Integer> accoppiataStudenteVoto : classe.getVoti().entrySet()){
            sommaVoti = sommaVoti + accoppiataStudenteVoto.getValue();
        }
        return sommaVoti / classe.getVoti().size();
    }
}
