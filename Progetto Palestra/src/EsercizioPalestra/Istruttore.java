package EsercizioPalestra;

import java.time.LocalDate;

public class Istruttore extends Membro{
    //Variabili di istanza
    private String specializzazione;
    private int anniEsperienza;

    /**
     * Costruttore dell'insegnante che tiene il corso
     *
     * @param nome è il nome dell'istruttore.
     * @param cognome è il cognome dell'istruttore.
     * @param idMembro è l'ID univoco legato all'istruttore.
     * @param abbonamento è l'attributto che viene dato ereditando membro. Si consiglia di metterlo a null.
     * @param dataIscrizione  l'attributto che viene dato ereditando membro. Si consiglia di metterlo a null.
     * @param abbonamentoValido  l'attributto che viene dato ereditando membro. Si consiglia di metterlo a null.
     * @param specializzazione è la specializzazione (disciplina sportiva) dell'istruttore.
     * @param anniEsperienza sono gli anni di esperienza dell'istruttore.
     */
    //Costruttore di classe
    public Istruttore(String nome, String cognome, String idMembro, TipologiaAbbonamento abbonamento, LocalDate dataIscrizione, boolean abbonamentoValido, String specializzazione, int anniEsperienza) {
        super(nome, cognome, idMembro, abbonamento, dataIscrizione, abbonamentoValido);
        this.specializzazione = specializzazione;
        this.anniEsperienza = anniEsperienza;
    }

    //Metodi Get
    public String getSpecializzazione() {
        return specializzazione;
    }
    public int getAnniEsperienza() {
        return anniEsperienza;
    }

    //Metodi Set
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }
    public void setAnniEsperienza(int anniEsperienza) {
        this.anniEsperienza = anniEsperienza;
    }
}
