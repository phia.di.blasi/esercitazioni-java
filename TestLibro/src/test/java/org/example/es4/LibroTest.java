package org.example.es4;

import org.example.es4.Libro;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LibroTest {
    static Libro libro;

    @BeforeAll
    static void setUp (){
        libro = new Libro("Cime Tempestose", "E. Bronte", 1847);
    }

    @Test
    void testCostruttoreLibro(){
        assertEquals("Cime Tempestose",libro.getTitolo());
        assertEquals("E. Bronte",libro.getAutore());
        assertEquals(1847,libro.getAnnoPubblicazione());
    }
}