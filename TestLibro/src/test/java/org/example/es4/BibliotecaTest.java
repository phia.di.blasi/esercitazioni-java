package org.example.es4;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class BibliotecaTest {
    static Biblioteca biblioteca;
    static Libro libro;

    @BeforeAll
    static void setUp(){
        biblioteca = new Biblioteca();
        libro = new Libro("Cime Tempestose", "E. Bronte", 1847);
    }

    @Test
    @org.junit.jupiter.api.Order(3)
    void testAggiuntaLibro(){
        assertEquals(0,biblioteca.getListaLibri().size());
        biblioteca.aggiungi(libro);
        assertEquals(1,biblioteca.getListaLibri().size());
    }
    @Test
    @org.junit.jupiter.api.Order(2)
    void testRicercaLibroEsistente(){
        biblioteca.aggiungi(libro);

        //2 metodi per testare la stessa cosa ma in modi diversi
        Libro libroTrovato = biblioteca.cerca("Cime Tempestose");
        assertEquals("Cime Tempestose",libroTrovato.getTitolo());
        assertEquals(libro,biblioteca.cerca(libro.getTitolo()));
    }
    @Test
    @org.junit.jupiter.api.Order(1)
    void testRicercaLibroNonEsistente(){
        //assertEquals(null,biblioteca.cerca(libro.getTitolo()));
        assertNull(biblioteca.cerca(libro.getTitolo()));
    }
}