package org.example.es4;

public class Main {
    public static void main(String[] args) {
        //Creo x ogg. Libro
        LibroDigitale libro1 = new LibroDigitale("Cime Tempestose", "E. Bronte", 1847,"PDF");
        LibroStampato libro2 = new LibroStampato("Cime Tempestose", "E. Bronte", 1847,800);
        LibroDigitale libro3 = new LibroDigitale( "Orgoglio e pregiudizio", "J. Austen", 1813,"PDF");
        LibroStampato libro4 = new LibroStampato( "Orgoglio e pregiudizio", "J. Austen", 1813,500);
        LibroDigitale libro5 = new LibroDigitale("Il fantasma di Canterville", "O. Wilde", 1887,"PDF");
        LibroStampato libro6 = new LibroStampato("Il fantasma di Canterville", "O. Wilde", 1887,420);

        Biblioteca biblioteca = new Biblioteca();

        biblioteca.aggiungi(libro1);
        biblioteca.aggiungi(libro2);
        biblioteca.aggiungi(libro3);
        biblioteca.aggiungi(libro4);
        biblioteca.aggiungi(libro5);
        biblioteca.aggiungi(libro6);

        Libro libroTrovato = biblioteca.cerca("Cime Tempestose");

        biblioteca.visualizzaTutti();

        int pagineTot = biblioteca.quantitaPagine();

        System.out.println(libroTrovato.getTitolo());
        System.out.println(pagineTot);

    }
}
