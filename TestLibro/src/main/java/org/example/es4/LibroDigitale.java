package org.example.es4;

public class LibroDigitale extends Libro {
    private String formato;

    public LibroDigitale(String titolo, String autore, int annoPubblicazione, String formato) {
        super(titolo, autore, annoPubblicazione);
        this.formato = formato;
    }
    public String getFormato() {
        return formato;
    }
}

