package org.example.es4;

public class LibroStampato extends Libro{
    private int numeroPagine;
    public LibroStampato(String titolo, String autore, int annoPubblicazione, int numeroPagine) {
        super(titolo, autore, annoPubblicazione);
        this.numeroPagine = numeroPagine;
    }
    public int getNumeroPagine() {
        return numeroPagine;
    }
}
