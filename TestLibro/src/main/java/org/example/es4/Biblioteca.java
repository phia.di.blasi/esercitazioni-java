package org.example.es4;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

    //Inizializzo la mia arraylist
    private List<Libro> listaLibri = new ArrayList<>();

    public List<Libro> getListaLibri() {
        return listaLibri;
    }

    //Metodi
    public void aggiungi(Libro nuovoLibro){
        this.listaLibri.add(nuovoLibro);
    }
    public Libro cerca(String titolo){
        Libro risultato = null;
        for (int i = 0; i < listaLibri.size(); i++) {
            Libro indexLibro = this.listaLibri.get(i);
            if (indexLibro.getTitolo().equals(titolo)) {
                risultato = indexLibro;
            }
        }
        return risultato;
    }
    public void visualizzaTutti (){
        for (int i = 0; i < listaLibri.size(); i++) {
            Libro indexLibro = this.listaLibri.get(i);
            System.out.println(indexLibro.getTitolo());
        }
    }
    public int quantitaPagine (){
        int numeroPagTot = 0;
        for (int i = 0; i < listaLibri.size(); i++) {
            Libro indexLibro = this.listaLibri.get(i);
            if (indexLibro instanceof LibroStampato) {
                numeroPagTot = numeroPagTot + ((LibroStampato) indexLibro).getNumeroPagine();
            }
        }
        return numeroPagTot;
    }
}
